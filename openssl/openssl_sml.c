#include <openssl/ossl_typ.h>
#include <openssl/ssl.h>
#include <openssl/err.h>
#include <openssl/asn1.h>

#include <string.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <unistd.h>
#include <netinet/in.h>

void OpenSSL_SML_init() {
  SSL_library_init();
}

void OpenSSL_SML_load_error_strings() {
  SSL_load_error_strings();
  ERR_load_X509_strings();
}

void OpenSSL_SML_load_BIO_strings() {
  ERR_load_BIO_strings();
}

int OpenSSL_SML_get_error() {
  return ERR_get_error();
}

int OpenSSL_SML_should_retry(BIO *b) {
  return BIO_should_retry(b);
}

const char *OpenSSL_SML_lib_error_string(int err) {
  return ERR_lib_error_string(err);
}

const char *OpenSSL_SML_func_error_string(int err) {
  return ERR_func_error_string(err);
}

const char *OpenSSL_SML_reason_error_string(int err) {
  return ERR_reason_error_string(err);
}

int OpenSSL_SML_read(BIO *b, void *data, int len) {
  return BIO_read(b, data, len);
}

int OpenSSL_SML_write(BIO *b, const void *data, int len) {
  return BIO_write(b, data, len);
}

BIO *OpenSSL_SML_new_connect(char *addr) {
  return BIO_new_connect(addr);
}

void OpenSSL_SML_free_all(BIO *b) {
  BIO_free_all(b);
}

int OpenSSL_SML_do_connect(BIO *b) {
  return BIO_do_connect(b);
}

int OpenSSL_SML_do_accept(BIO *b) {
  return BIO_do_accept(b);
}

int OpenSSL_SML_do_handshake(BIO *b) {
  return BIO_do_handshake(b);
}

SSL_CTX *OpenSSL_SML_CTX_new(SSL_METHOD *meth) {
  SSL_CTX *ctx = SSL_CTX_new(meth);
  SSL_CTX_set_verify(ctx,
		     SSL_VERIFY_PEER | SSL_VERIFY_FAIL_IF_NO_PEER_CERT,
		     0);
  SSL_CTX_set_verify_depth(ctx,
			   2);
  return ctx;
}

void OpenSSL_SML_CTX_free(SSL_CTX *ctx) {
  return SSL_CTX_free(ctx);
}

const SSL_METHOD *OpenSSL_SML_SSLv23_method() {
  return SSLv23_method();
}

int OpenSSL_SML_load_verify_locations(SSL_CTX *ctx, const char *trust, const char *certs) {
  return SSL_CTX_load_verify_locations(ctx, trust, certs);
}

BIO *OpenSSL_SML_new_ssl_connect(SSL_CTX *ctx) {
  BIO *bio = BIO_new_ssl_connect(ctx);
  SSL *ssl;

  BIO_get_ssl(bio, &ssl);
  SSL_set_mode(ssl, SSL_MODE_AUTO_RETRY);

  return bio;
}

BIO *OpenSSL_SML_new_ssl(SSL_CTX *ctx) {
  BIO *bio = BIO_new_ssl_connect(ctx);
  SSL *ssl;

  BIO_get_ssl(bio, &ssl);
  SSL_set_mode(ssl, SSL_MODE_AUTO_RETRY);

  return bio;
}

BIO *OpenSSL_SML_new_accept(SSL_CTX *ctx, char *port) {
  BIO *sbio = BIO_new_ssl(ctx, 0);
  BIO *acpt = BIO_new_accept(port);

  BIO_set_accept_bios(acpt, sbio);

  return acpt;
}

SSL *OpenSSL_SML_get_ssl(BIO *bio) {
  SSL *ssl;

  if (BIO_get_ssl(bio, &ssl) <= 0)
    return NULL;
  else
    return ssl;
}

int OpenSSL_SML_set_conn_hostname(BIO *bio, char *hostname) {
  BIO_set_conn_hostname(bio, hostname);
}

int OpenSSL_SML_set_accept_port(BIO *bio, char *port) {
  BIO_set_accept_port(bio, port);
}

int OpenSSL_SML_tcp_listen(int port, int qsize) {
  int sock;
  struct sockaddr_in sin;
  int val=1;
    
  if((sock=socket(AF_INET,SOCK_STREAM,0))<0)
    return -1;
    
  memset(&sin,0,sizeof(sin));
  sin.sin_addr.s_addr=INADDR_ANY;
  sin.sin_family=AF_INET;
  sin.sin_port=htons(port);
  setsockopt(sock,SOL_SOCKET,SO_REUSEADDR,
	     &val,sizeof(val));
    
  if(bind(sock,(struct sockaddr *)&sin,
	  sizeof(sin))<0)
    return -1;
  listen(sock, qsize);  

  return sock;
}

int OpenSSL_SML_accept(int sock) {
  return accept(sock, 0, 0);
}

BIO *OpenSSL_SML_new_socket(int sock) {
  return BIO_new_socket(sock, BIO_NOCLOSE);
}

SSL *OpenSSL_SML_SSL_new(SSL_CTX *ctx) {
  SSL *ssl = SSL_new(ctx);

  return ssl;
}

int OpenSSL_SML_SSL_shutdown(SSL *ssl) {
  return SSL_shutdown(ssl);
}

void OpenSSL_SML_shutdown(int sock) {
  shutdown(sock, 1);
}

void OpenSSL_SML_SSL_set_bio(SSL *ssl, BIO *b1, BIO *b2) {
  SSL_set_mode(ssl, SSL_MODE_AUTO_RETRY);
  SSL_set_bio(ssl, b1, b2);
  BIO_set_ssl(b1, ssl, BIO_NOCLOSE);
}

int OpenSSL_SML_use_PrivateKey_file(SSL_CTX *ctx, char *keyfile) {
  return SSL_CTX_use_PrivateKey_file(ctx,
				     keyfile,
				     SSL_FILETYPE_PEM);
}

int OpenSSL_SML_SSL_accept(SSL *ssl) {
  return SSL_accept(ssl);
}

int OpenSSL_SML_use_certificate_chain_file(SSL_CTX *ctx, char *keyfile) {
  return SSL_CTX_use_certificate_chain_file(ctx,
					    keyfile);
}

const char *OpenSSL_SML_get_peer_name(SSL *ssl) {
  X509 *x = SSL_get_peer_certificate(ssl);

  if (x) {
    X509_NAME *name = X509_get_subject_name(x);

    if (name) {
      X509_NAME_ENTRY *ne;

      ne = X509_NAME_get_entry(name, 0);

      if (ne) {
	ASN1_STRING *s = X509_NAME_ENTRY_get_data(ne);
	static char ret[1024];

	if (ASN1_STRING_length(s) >= sizeof ret)
	  return NULL;
	else {
	  memcpy(ret, ASN1_STRING_get0_data(s), ASN1_STRING_length(s));
	  ret[ASN1_STRING_length(s)] = 0;
	  return ret;
	}
      } else {
	printf("Null entry\n");
	return NULL;
      }
    } else {
      printf("Null name\n");
      return NULL;
    }
  } else {
    printf("Null certificate\n");
    return NULL;
  }
}

BIO *OpenSSL_SML_pop(BIO *b) {
  return BIO_pop(b);
}

BIO *OpenSSL_SML_next(BIO *b) {
  return BIO_next(b);
}

int OpenSSL_SML_puts(BIO *b, const char *buf) {
  return BIO_puts(b, buf);
}
