{{The most common kinds of domain configuration}}

val web_node : (web_node) = "shelob";
val default_node : (node) = web_node_to_node web_node;
val web_ip = ip_of_node (web_node_to_node web_node);
val web_ipv6 = ipv6_of_node (web_node_to_node web_node);

var WebPlaces = [web_place_default web_node];

val webAt =
  \ n : (web_node) ->
  \ host : (host) ->
  \\ config : Vhost -> begin
    dns (dnsA (literal host) (ip_of_node (web_node_to_node n)));
    dns (dnsAAAA (literal host) (ipv6_of_node (web_node_to_node n)));

    vhost host where
      WebPlaces = [web_place_default n]
    with
      config
    end
  end;

val web = \ host -> \\ config : Vhost -> begin
  n <- DefaultWebNode;
  webAt n host with config end;
end;

val webAtIp =
  \ ip : (your_ip) ->
  \ ipv6 : (your_ipv6) ->
  \ host : (host) ->
  \\ config : Vhost -> begin
    dns (dnsA (literal host) (your_ip_to_ip ip));
    dns (dnsAAAA (literal host) (your_ipv6_to_ipv6 ipv6));

    vhost host where
      WebPlaces = [web_place web_node ip ipv6]
    with
      config
    end
  end;

val webSsl = \ host -> \ certFile -> \\ config : Vhost -> begin
  n <- DefaultWebNode;
  webAt n host where
    SSL = certFile;
  with config end;

  force_ssl <- ForceSSL;
  if force_ssl then
    webAt n host where
      SSL = no_ssl;
    with
      rewriteRule "^(.*)$" "https://%{HTTP_HOST}$1" [redirect];

      config; (* config is included in case of directives like
                 serverAliasDefault. https rewrite should override all
                 others since it matches first. *)
    end;
  else
    webAt n host where
      SSL = no_ssl;
    with config end;
  end;
end;

val addDefaultAlias = begin
  mailbox <- Mailbox;
  source <- DefaultAliasSource;
  aliasPrim source (addressTarget mailbox)
end;

val addWww = begin
  web "www" with
    serverAliasDefault;
    www : [Vhost] <- WWW;
    www
  end
end;

val dom =
  \ d : (your_domain) ->
  \\ config : Domain ->
  domain d with
    dns (dnsNS "ns1.hcoop.net");
    dns (dnsNS "ns2.hcoop.net");

    user_web_node <- DefaultWebNode;

    defa : bool <- DefaultA;
    if defa then
      dns (dnsA default (ip_of_node (web_node_to_node user_web_node)));
      dns (dnsAAAA default (ipv6_of_node (web_node_to_node user_web_node)))
    else
      Skip
    end;

    hmail : bool <- HandleMail;
    if hmail then
      handleMail
    else
      Skip
    end;

    amx : bool <- AddMX;
    if amx then
      dns (dnsMX 1 "mail.hcoop.net");
    else
      Skip
    end;

    createWWW : bool <- CreateWWW;
    if createWWW then
      addWww
    else
      Skip
    end;

    defAl : bool <- DefaultAlias;
    if defAl then
      addDefaultAlias
    else
      Skip
    end;

    config
end;

val nameserver = \host -> dns (dnsNS host);
val dnsIP = \from -> \to -> dns (dnsA (literal from) to);
val dnsIPv6 = \from -> \to -> dns (dnsAAAA (literal from) to);
val dnsMail = \num -> \host -> dns (dnsMX num host);
val dnsAlias = \from -> \to -> dns (dnsCNAME (literal from) to);
val dnsDefault = \to -> dns (dnsA default to);
val dnsDefaultv6 = \to -> dns (dnsAAAA default to);
val dnsText = \from -> \to -> dns (dnsTXT (srv_literal from) to);
val dnsDefaultText = \to -> dns (dnsTXT srv_default to);

val dnsWildcardIP = \to -> dns (dnsA wildcard to);

val addDefaultSPF = dnsDefaultText "v=spf1 mx -all";
