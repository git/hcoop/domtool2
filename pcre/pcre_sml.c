#include <pcre.h>

int PCRE_SML_validRegexp(const char *s) {
  pcre *re;
  const char *error;
  int erroffset;

  re = pcre_compile(s, 0, &error, &erroffset, NULL);
  
  if (re) {
    pcre_free(re);
    return 1;
  } else
    return 0;
}
