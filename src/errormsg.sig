(* This file comes mostly from "Modern Compiler Implementation in ML," by Andrew Appel
 * http://www.cs.princeton.edu/~appel/modern/ml/
 *)

signature ERRORMSG =
  sig
    val reset : unit -> unit
      
    val anyErrors : bool ref
      
    val fileName : string ref
    val sourceStream : TextIO.instream ref
      
    val lineNum : int ref
    val linePos : int list ref

    val error : (int * int) option -> string -> unit
    val warning : (int * int) option -> string -> unit

    val dummyLoc : int * int

    exception Error
end
