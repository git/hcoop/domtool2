(* HCoop Domtool (http://hcoop.sourceforge.net/)
 * Copyright (c) 2006, Adam Chlipala
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 *)

(* Webalizer statistics tool handling *)

structure Webalizer :> WEBALIZER = struct

val files = ref ([] : TextIO.outstream list)
val write = ref (fn _ : string => ())

val () = Apache.registerPre
	 (fn {user, nodes, id, hostname} =>
	     let
		 val fds = map (fn node =>
				   let
				       val fd = Domain.domainFile {node = node,
								   name = id ^ ".wbl"}
				   in
				       TextIO.output (fd, "LogFile\t");
				       TextIO.output (fd, Apache.realLogDir {user = user, node = node, vhostId = hostname});
				       TextIO.output (fd, "/access.log\nOutputDir\t");
				       TextIO.output (fd, Config.Webalizer.outputDir);
				       TextIO.output (fd, "/");
				       TextIO.output (fd, node);
				       TextIO.output (fd, "/");
				       TextIO.output (fd, id);
				       TextIO.output (fd, "\n");
				       fd
				   end) nodes
	     in
		 files := fds;
		 write := (fn s => app (fn fd => TextIO.output (fd, s)) fds);
		 !write "HostName\t";
		 !write hostname;
		 !write "\nHideSite\t";
		 !write hostname;
		 !write "\nHideReferrer\t";
		 !write hostname;
		 !write "\n"
	     end)

val () = Apache.registerPost
	 (fn () => app TextIO.closeOut (!files))

val () = Apache.registerAliaser
	 (fn hostname =>
	     (!write "HideSite\t";
	      !write hostname;
	      !write "\nHideReferrer\t";
	      !write hostname;
	      !write "\n"))

val () = Slave.registerFileHandler (fn fs =>
				       let
					   val spl = OS.Path.splitDirFile (#file fs)
				       in
					   case OS.Path.splitBaseExt (#file spl) of
					       {base, ext = SOME "wbl"} =>
					       let
						   fun backupDir () = OS.Path.joinDirFile
									  {dir = Config.Webalizer.backupDir,
									   file = base}
					       in
						   case #action fs of
						       Slave.Delete _ =>
						       (ignore (OS.Process.system (Config.rm
										   ^ " -f "
										   ^ Config.Webalizer.configDir
										   ^ "/"
										   ^ Slave.hostname ()
										   ^ "/"
										   ^ base
										   ^ ".conf"));
							Slave.moveDirCreate {from = Config.Webalizer.outputDir
										    ^ "/"
										    ^ Slave.hostname ()
										    ^ "/"
										    ^ base,
									     to = backupDir ()})
						     | _ =>
						       (ignore (OS.Process.system (Config.cp
										   ^ " "
										   ^ #file fs
										   ^ " "
										   ^ Config.Webalizer.configDir
										   ^ "/"
										   ^ Slave.hostname ()
										   ^ "/"
										   ^ base
										   ^ ".conf"));
							let
							    val dir = Config.Webalizer.outputDir ^ "/" ^ Slave.hostname () ^ "/" ^ base
							in
							    if Posix.FileSys.access (dir, []) then
								()
							    else
								Slave.moveDirCreate {from = backupDir (),
										     to = dir}
							end)
					       end
					     | _ => ()
				       end)

val () = Domain.registerResetLocal (fn () =>
				       app (fn (node, _, _) =>
					       ignore (OS.Process.system (Config.rm ^ " -rf "
									  ^ Config.Webalizer.configDir ^ "/"
									  ^ node ^ "/*"))) Config.nodeIps)

val () = Domain.registerDescriber (Domain.considerAll
				   [Domain.Extension {extension = "wbl",
						      heading = fn host => "Webalizer config for " ^ host ^ ":"}])

end
