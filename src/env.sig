(* HCoop Domtool (http://hcoop.sourceforge.net/)
 * Copyright (c) 2006, Adam Chlipala
 * Copyright (c) 2014 Clinton Ebadi <clinton@unknownlamer.org>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 *)

(* Domtool type-checking and reduction environments *)

signature ENV = sig

    type typeRule = Ast.exp -> bool
    val registerType : string * typeRule -> unit
    val typeRule : string -> typeRule option

    type env_vars = Ast.exp Ast.StringMap.map
    type action = env_vars * Ast.exp list -> env_vars
    val registerAction : string * action -> unit
    val action : string -> action option

    val registerContainer : string * action * (unit -> unit) -> unit
    val container : string -> (action * (unit -> unit)) option

    (* Actions to take before and after evaluating a file *)
    val registerPre : (unit -> unit) -> unit
    val pre : unit -> unit
    val registerPost : (unit -> unit) -> unit
    val post : unit -> unit

    (* ...and before type-checking *)
    val registerPreTycheck : (unit -> unit) -> unit
    val preTycheck : unit -> unit

    val badArgs : string * Ast.exp list -> 'a
    val badArg : string * string * Ast.exp -> 'a

    type 'a arg = Ast.exp -> 'a option

    val int : int arg
    val string : string arg
    val bool : bool arg
    val list : 'a arg -> 'a list arg

    val none : string -> (unit -> unit) -> action
    val one : string -> string * 'a arg -> ('a -> unit) -> action
    val two : string -> string * 'a arg * string * 'b arg -> ('a * 'b -> unit) -> action
    val three : string
		-> string * 'a arg * string * 'b arg * string * 'c arg
		-> ('a * 'b * 'c -> unit) -> action

    val noneV : string -> (env_vars -> unit) -> action
    val oneV : string -> string * 'a arg -> (env_vars * 'a -> unit) -> action
    val twoV : string -> string * 'a arg * string * 'b arg -> (env_vars * 'a * 'b -> unit) -> action

    val env : 'a arg -> env_vars * string -> 'a

    val type_one : string -> 'a arg -> ('a -> bool) -> unit

    val action_none : string -> (unit -> unit) -> unit
    val action_one : string -> string * 'a arg -> ('a -> unit) -> unit
    val action_two : string -> string * 'a arg * string * 'b arg -> ('a * 'b -> unit) -> unit
    val action_three : string
		       -> string * 'a arg * string * 'b arg * string * 'c arg
		       -> ('a * 'b * 'c -> unit) -> unit
    val action_four : string
		       -> string * 'a arg * string * 'b arg * string * 'c arg * string * 'd arg
		       -> ('a * 'b * 'c * 'd -> unit) -> unit

    val actionV_none : string -> (env_vars -> unit) -> unit
    val actionV_one : string -> string * 'a arg -> (env_vars * 'a -> unit) -> unit
    val actionV_two : string -> string * 'a arg * string * 'b arg -> (env_vars * 'a * 'b -> unit) -> unit

    val container_none : string -> (unit -> unit) * (unit -> unit) -> unit
    val container_one : string -> string * 'a arg -> ('a -> unit) * (unit -> unit) -> unit

    val containerV_none : string -> (env_vars -> unit) * (unit -> unit) -> unit
    val containerV_one : string -> string * 'a arg -> (env_vars * 'a -> unit) * (unit -> unit) -> unit

    val registerFunction : string * (Ast.exp list -> Ast.exp option) -> unit
    val function : string -> (Ast.exp list -> Ast.exp option) option

    type env
    val empty : env

    val initialDynEnvTypes : env -> Ast.typ Ast.StringMap.map
    val initialDynEnvVals : (env -> Ast.exp -> Ast.exp) -> env -> env_vars

    val bindType : env -> string -> env
    val bindVal : env -> string * Ast.typ * Ast.exp option -> env
    val bindContext : env -> string -> env
    val bindInitialDynEnvVal : env -> string * Ast.typ * Ast.exp -> env

    val lookupType : env -> string -> bool
    val lookupVal : env -> string -> Ast.typ option
    val lookupEquation : env -> string -> (Ast.exp * env) option
    val lookupContext : env -> string -> bool
    val lookupInitialDynEnvVal : env -> string -> Ast.typ option

    val types : env -> Ast.StringSet.set
    val vals : env -> Ast.StringSet.set
    val contexts : env -> Ast.StringSet.set
    val dynamics : env -> Ast.StringSet.set
end
