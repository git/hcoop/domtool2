(* HCoop Domtool (http://hcoop.sourceforge.net/)
 * Copyright (c) 2006, Adam Chlipala
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 *)

(* DBMS management code *)

structure Dbms :> DBMS = struct

open DataStructures

val validDbname = CharVector.all (fn ch => Char.isAlphaNum ch orelse ch = #"_")
fun validEncoding encoding =
    case encoding of
	NONE => true
      | SOME e => size e > 0 andalso size e < 20 andalso CharVector.all Char.isAlphaNum e

type handler = {getpass : (unit -> Client.passwd_result) option,
		adduser : {user : string, passwd : string option} -> string option,
		passwd : {user : string, passwd : string} -> string option,
		createdb : {user : string, dbname : string, encoding : string option} -> string option,
		dropdb : {user : string, dbname : string} -> string option,
		grant : {user : string, dbname : string} -> string option}
	       
val dbmses : handler StringMap.map ref = ref StringMap.empty
		  
fun register (name, handler) =
    dbmses := StringMap.insert (!dbmses, name, handler)

fun lookup name = StringMap.find (!dbmses, name)

end
