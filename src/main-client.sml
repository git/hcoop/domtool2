(* HCoop Domtool (http://hcoop.sourceforge.net/)
 * Copyright (c) 2006, Adam Chlipala
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 *)

(* Driver for configuration requests *)

val () = Domain.declareClient ()

fun uid () =
    case Posix.ProcEnv.getenv "DOMTOOL_USER" of
	NONE => Posix.ProcEnv.getuid ()
      | SOME user => Posix.SysDB.Passwd.uid (Posix.SysDB.getpwnam user)

fun domtoolRoot () =
    let
	val dname = Posix.SysDB.Passwd.home (Posix.SysDB.getpwuid (uid ()))
    in
	OS.Path.joinDirFile {dir = dname,
			     file = ".domtool"}
    end

fun libnameOpt () =
    let
	val libname = OS.Path.joinDirFile {dir = domtoolRoot (),
					   file = "lib.dtl"}
    in
	if Posix.FileSys.access (libname, []) then
	    SOME libname
	else
	    NONE
    end

val (tc, fake, args) = foldl (fn (arg, (tc, fake, args)) =>
				 case arg of
				     "-tc" => (true, fake, args)
				   | "-fake" => (tc, true, args)
				   | _ => (tc, fake, arg :: args))
		       (false, false, []) (CommandLine.arguments ())

val args = rev args

val (doit, doitDir) =
    if tc then
	(fn fname =>
	    let
		val _ : string = Main.setupUser ()
		val () = if fake then
			     Domain.fakePrivileges ()
			 else
			     ()
		val env = Main.basis ()
		val env =
		    case libnameOpt () of
			NONE => env
		      | SOME libname => #1 (Main.check env libname)
	    in
		ignore (Main.check env fname)
	    end,
	 Main.checkDir)
    else
	(fn fname => Main.request (fname, libnameOpt ()),
	 Main.requestDir)

val _ =
    case args of
	[fname] =>
	if Posix.FileSys.access (fname, []) then
	    doit fname
	else
	    doit (OS.Path.joinDirFile {dir = domtoolRoot (),
				       file = fname})
      | [] => doitDir (domtoolRoot ())
      | _ => print "Invalid command-line arguments\n"
