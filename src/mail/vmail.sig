(* HCoop Domtool (http://hcoop.sourceforge.net/)
 * Copyright (c) 2006, Adam Chlipala
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 *)

(* Administration of Courier IMAP virtual mailboxes *)

signature VMAIL = sig

    val rebuild : unit -> bool

    datatype listing =
	     Error of string
	   | Listing of {user : string, mailbox : string} list

    val list : string -> listing

    val mailboxExists : {domain : string, user : string} -> bool

    val add : {domain : string, requester : string, user : string,
	       passwd : string, mailbox : string} -> string option

    val passwd : {domain : string, user : string, passwd : string}
		 -> string option

    val portalpasswd : {domain : string, user : string, oldpasswd : string, newpasswd : string}
		       -> string option

    val rm : {domain : string, user : string} -> string option

    val doChanged : unit -> bool

end
