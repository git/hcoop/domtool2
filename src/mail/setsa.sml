(* HCoop Domtool (http://hcoop.sourceforge.net/)
 * Copyright (c) 2006, Adam Chlipala
 * Copyright (c) 2014 Clinton Ebadi <clinton@unknownlamer.org>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 *)

(* Setting SpamAssassin filtering preferences *)

structure SetSA :> SET_SA = struct

open MsgTypes

datatype address =
	 User of string
       | Email of string

fun address (User s) = s ^ "@localhost"
  | address (Email s) = s

fun file addr = OS.Path.joinDirFile {dir = Config.SpamAssassin.addrsDb,
				     file = address addr}

fun query addr = Posix.FileSys.access (file addr, [])

fun set (addr, setting) =
    if setting then
	TextIO.closeOut (TextIO.openAppend (file addr))
    else
	OS.FileSys.remove (file addr)

fun rebuild () =
    let
	fun doNode (site, ok) =
	    (print ("New spamassassin data for node " ^ site ^ "\n");
	     Connect.commandWorker (Domain.get_context (), site, MsgSaChanged))
    in
	foldl doNode true (Config.mailNodes_all @ Config.mailNodes_admin)
    end

end
