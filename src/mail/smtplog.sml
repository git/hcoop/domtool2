(* HCoop Domtool (http://hcoop.sourceforge.net/)
 * Copyright (c) 2006, Adam Chlipala
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 *)

(* Selective viewing of SMTP logs *)

structure SmtpLog :> SMTP_LOG = struct

fun search handler domain =
    let
	val proc = Unix.execute (Config.sudo,
				 [Config.domtool_publish,
				  "smtplog",
				  String.translate (fn #"." => "\\."
						     | ch => str ch) domain])

	val inf = Unix.textInstreamOf proc

	fun loop () =
	    case TextIO.inputLine inf of
		NONE => ()
	      | SOME line => (handler line;
			      loop ())
    in
	loop ()
	before (TextIO.closeIn inf;
		ignore (Unix.reap proc))
end

end
