(* HCoop Domtool (http://hcoop.sourceforge.net/)
 * Copyright (c) 2006, Adam Chlipala
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 *)

(* Code for receiving and executing configuration files *)

signature SLAVE = sig

    datatype file_action =
	     Add
	   | Delete of bool (* Set to true to really delete the file *)
	   | Modify

    val isDelete : file_action -> bool

    type file_status = {action : file_action,
			domain : string,
			dir : string,
			file : string}

    val registerFileHandler : (file_status -> unit) -> unit
    (* Register a function to be called when a result configuration file's
     * status has changed. Registered handlers are called in the reverse order
     * from registration order. *)

    val registerPreHandler : (unit -> unit) -> unit
    val registerPostHandler : (unit -> unit) -> unit
    (* Register code to run before or after making all changes. *)

    val handleChanges : file_status list -> unit

    val shell : string list -> bool
    val shellF : string list * (string -> string) -> unit
    val shellOutput : string list -> string option
    val run : string * string list -> bool
    val runOutput : string * string list -> bool * string option
    (* Like shellOutput, but return status and lines and don't use the shell *)

    val concatTo : (string -> bool) -> string -> unit
    (* Search through the result configuration hierarchy for all files matching
     * the predicate, concatenating their contents in arbitrary order to the
     * given file. *)

    val enumerateTo : (string -> bool) -> string -> string -> unit
    (* Search through the result configuration hierarchy for all files matching
     * the predicate, writing a list of their domains to the file named by the
     * third argument, delimiting the entries with the second argument. *)

    val hostname : unit -> string
    (* Get hostname of this machine *)

    val readList : string -> string list
    val writeList : string * string list -> unit
    (* Reading and writing lists of strings stored on separate lines in files *)

    val lineInFile : string -> string -> bool
    (* Is there a line in the file (first arg) that matches that given? *)

    val inGroup : {user : string, group : string} -> bool
    (* Check membership in a UNIX group. *)

    val mkDirAll : string -> unit
    (* [mkDirAll p] creates directory "p", creating all parent directories, as
     * necessary. *)

    val remove : ''a list * ''a -> ''a list
    val removeDups : ''a list -> ''a list

    val moveDirCreate : { from : string, to : string } -> unit
end
